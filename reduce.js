function reduce(elements, cb, startingValue) {
    if (!elements || !cb || !Array.isArray(elements)) {
        return;
    }
    let value = startingValue;
    for (let index = 1; index < elements.length; index++) {

        value = cb(value, elements[index], index, elements)

    }
    return value;
}

module.exports = reduce;
