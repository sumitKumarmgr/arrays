function flatten(elements, depth) {
    if (depth == null) {
        return flattenAlways(elements, [])
    } else {
        return flattenDepth(elements, [], depth)
    }
    function flattenAlways(elements, result) {
        for (let i = 0; i < elements.length; i++) {
            let value = elements[i]
            if (Array.isArray(value)) {
                flattenAlways(value, result)
            } else if (value === undefined) {
                continue;
            }
            else {
                result.push(value)
            }
        }
        return result
    }
    function flattenDepth(elements, result, depth) {
        for (let i = 0; i < elements.length; i++) {
            let value = elements[i]

            if (depth > 0 && Array.isArray(value)) {
                flattenDepth(value, result, depth - 1)
            } else if (value === undefined) {
                continue;
            }
            else {
                result.push(value)
            }
        }
        return result
    }
}

module.exports = flatten;
